NCRELEASE=$1

apt-get update && apt-get dist-upgrade --assume-yes
wget --quiet https://download.nextcloud.com/server/releases/$NCRELEASE.zip
wget --quiet wget-$NCRELEASE-zip-asc https://download.nextcloud.com/server/releases/$NCRELEASE.zip.asc
wget --quiet wget-nextcloud-asc https://nextcloud.com/nextcloud.asc
gpg --import nextcloud.asc

apt-get install apache2 mariadb-server libapache2-mod-php7.3 --assume-yes
apt-get install php7.3-gd php7.3-mysql php7.3-curl php7.3-mbstring php7.3-intl --assume-yes
apt-get install php7.3-gmp php7.3-bcmath php-imagick php7.3-xml php7.3-zip unzip --assume-yes

cat << EOF > /etc/apache2/sites-available/nextcloud.conf
<VirtualHost *:443>
  DocumentRoot /var/www/nextcloud/
  ServerName  your.server.com
  ErrorLog ${APACHE_LOG_DIR}/error.log
  CustomLog ${APACHE_LOG_DIR}/access.log combined
  SSLEngine on
  SSLCertificateFile      /etc/ssl/certs/ssl-cert-snakeoil.pem
  SSLCertificateKeyFile /etc/ssl/private/ssl-cert-snakeoil.key
  <FilesMatch "\.(cgi|shtml|phtml|php)$">
  	SSLOptions +StdEnvVars
  </FilesMatch>
  <Directory /usr/lib/cgi-bin>
  	SSLOptions +StdEnvVars
  </Directory>
  <Directory /var/www/nextcloud/>
    Require all granted
    AllowOverride All
    Options FollowSymLinks MultiViews

    <IfModule mod_dav.c>
      Dav off
    </IfModule>
  </Directory>
</VirtualHost>
EOF

a2dissite 000-default.conf
a2ensite nextcloud.conf
a2enmod rewrite
a2enmod headers
a2enmod env
a2enmod dir
a2enmod mime
a2enmod ssl
systemctl reload apache2
# create random password
DBPASS="$(openssl rand -base64 12)"
# database and user name
DBNAME="nextcloud"
# create DB
mysql -e "CREATE DATABASE ${DBNAME} /*\!40100 DEFAULT CHARACTER SET utf8 */;"
mysql -e "CREATE USER ${DBNAME}@localhost IDENTIFIED BY '${DBPASS}';"
mysql -e "GRANT ALL PRIVILEGES ON ${DBNAME}.* TO '${DBNAME}'@'localhost';"
mysql -e "FLUSH PRIVILEGES;"

unzip $NCRELEASE.zip
cp -r nextcloud /var/www
chown -R www-data:www-data /var/www/nextcloud/

# set PHP options
sed -i 's/memory_limit = .*/memory_limit = 1024M/' /etc/php/7.3/apache2/php.ini
systemctl reload apache2

ADMINPASS="$(openssl rand -base64 16)"

gpg --verify $NCRELEASE.zip.asc

cd /var/www/nextcloud/
sudo -u www-data php occ  maintenance:install --database \
"mysql" --database-name "$DBNAME"  --database-user "$DBNAME" --database-pass \
"$DBPASS" --admin-user "admin" --admin-pass "$ADMINPASS"

SERVERIP=$(hostname -I)

sudo -u www-data php occ config:system:set trusted_domains 1 --value=$SERVERIP

echo "------------------------------------------------------"
echo "server address:         " $SERVERIP
echo "database name:          " $DBNAME
echo "database user login:    " $DBNAME
echo "database user password: " $DBPASS
echo "admin password:         " $ADMINPASS
